// alert("hellow")
// console.log("im here")
//
//initialization
let numberA = null;
let numberB = null;
let operation = null;

//to retrieve an element from the webpage, we can use querySelector
let inputDisplay = document.querySelector('#txt-input-display')
//the queryselector function takes a string input that is formatted like a css selector. 
//this allows us to get a specific element such as css selectors
//alternatively, pwede rin ung document.getElementById
////naka all siya kasi lahat ng may class name ng .btn-numbers kukunin niya
//inassign lang yang mga color white
let btnNumbers = document.querySelectorAll('.btn-numbers');
let btnAdd = document.querySelector('#btn-add');
let btnSubtract = document.querySelector('#btn-subtract');
let btnMultiply = document.querySelector('#btn-multiply');
let btnDivide = document.querySelector('#btn-divide');
let btnEqual = document.querySelector('#btn-equal');
let btnDecimal = document.querySelector('#btn-decimal');
let btnClearAll = document.querySelector('#btn-clear-all');
let btnBackspace = document.querySelector('#btn-backspace');
let firstName = document.getElementById('fnameInput');
let lastName = document.getElementById('lnameInput');
let fullName = document.getElementById('fullname');
let fullNameOutput = document.getElementById('fullNameOutput');



btnNumbers.forEach(function(btnNumber){
	btnNumber.onclick =() =>{ //may gusto kasi tayong ipagawa sa onclick
		inputDisplay.value += btnNumber.textContent; //para ipakita kung anong number ung pinakita niya
	}
});

btnAdd.onclick = () => {
	if (numberA == null){
		numberA = Number(inputDisplay.value);
		operation = 'addition';
		inputDisplay.value = null;
	}else if (numberB == null){
		numberB = Number(inputDisplay.value);
		numberA = numberA + numberB;
		operation = 'addition';
		numberB.value = null;
		inputDisplay.value = null;
	}
}
btnSubtract.onclick = () => {
	if (numberA == null){
		numberA = Number(inputDisplay.value);
		operation = 'subtraction';
		inputDisplay.value = null;
	}else if (numberB == null){
		numberB = Number(inputDisplay.value);
		numberA = numberA - numberB;
		operation = 'subtraction';
		numberB.value = null;
		inputDisplay.value = null;
	}
}
btnMultiply.onclick = () => {
	if (numberA == null){
		numberA = Number(inputDisplay.value);
		operation = 'multiplication';
		inputDisplay.value = null;
	}else if (numberB == null){
		numberB = Number(inputDisplay.value);
		numberA = numberA * numberB;
		operation = 'multiplication';
		numberB.value = null;
		inputDisplay.value = null;
	}
}
btnDivide.onclick = () => {
	if (numberA == null){
		numberA = Number(inputDisplay.value);
		operation = 'division';
		inputDisplay.value = null;
	}else if (numberB == null){
		numberB = Number(inputDisplay.value);
		numberA = numberA / numberB;
		operation = 'division';
		numberB.value = null;
		inputDisplay.value = null;
	}
}


btnEqual.onclick = () => {
	if (numberB == null  && inputDisplay.value !== ''){
		numberB = inputDisplay.value;
	}
	if (operation == 'addition'){
		inputDisplay.value =  Number(numberA) + Number(numberB);
	}
	if (operation == 'subtraction'){
		inputDisplay.value =  Number(numberA) - Number(numberB);
	}
	if (operation == 'multiplication'){
		inputDisplay.value =  Number(numberA) * Number(numberB);
	}
	if (operation == 'division'){
		inputDisplay.value =  Number(numberA) / Number(numberB);
	}

}
btnClearAll.onclick = () => {
	numberA = null;
	numberB = null;
	operation = null;
	inputDisplay.value = null;
}
btnBackspace.onclick = () => {
	inputDisplay.value = inputDisplay.value.slice(0,-1)
}

btnDecimal.onclick = () => {
	if (!inputDisplay.value.includes('.')) {
		inputDisplay.value = inputDisplay.value + btnDecimal.textContent;
	}
}


//onkeyup - okay siya gamitin since gumagamit ng keyboard pag nag ta-type bale kung ano tinype ni user sa keyboard, yun kukunin niya..
//
firstName.onkeyup = () => {
	document.getElementById('firstNameOutput').innerHTML = firstName.value;//yung color yellow 
	//(firstNameOutput) is id na index.html ko. firstName naman is yung sinet kong variable kay js
}

lastName.onkeyup = () => {
	document.getElementById('lastNameOutput').innerHTML = lastName.value;
}


//btnAdd
//btnSubtract
//btnMultiply
//btnDivide
//btnEqual
//btnDecimal
////btnClearAll
///btnBackspace